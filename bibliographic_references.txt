# column 1: type
# column 2: title
# column 3: authors
# column 4: year
journal	Ann. Se. nat. 5e s�r.
journal	Bull. Jard. Bot. Bruxelles
journal	Bull. Jard. Bot. �tat Bruxelles
journal	Bull. Jard. Bot. �tat Brux.
journal	Bot. Jahrb.
journal	Bull. Soc. Bot. France
journal	Nat. Pflanzenf.
journal	Pflanzenf.
journal	Kew Bull.
journal	Journ. linn. Soc.
journal	Journ. Agrie. Research
book	M�moires Mus. Hist. Nat. Paris 12	A. de Jussieu	1825
book	Fl. for. C�te d'Ivoire 2e �d.	Aubr�ville	1959
book	Fl. for. soudano-guin�enne	Aubr�ville	1950
book	Plantes alimentaires 2 (Fruits)	Bois	1928
book	Miss. et. for. col. 1, Bois C�te d'Ivoire	Bertin	1918
book	Rev. Bot. appl. i5	Chevalier	1935
book	V�g. ut. Afr. trop, franc. 5, Bois C�te d'Ivoire	Chevalier	1909
book	V�g. ut. Afr. trop, franc. 9, For�t et Bois du Gabon	Chevalier	1917
book	Ann. Mus�e Congo 5, �tudes Fl. Bas et Moyen Congo	De Wildeman	1906
book	Consp. flor, angol.	Exell et Mendon�a	1951
book	Pl. util. Afr. port.	Ficalho	1884
book	Fl. Congo belge	Gilbert	1958
book	Fl. West trop. Afr. ire �d.	Hutchinson et Dalziel	1928
book	Fl. West trop. Afr. 2e �d.	Hutchinson et Dalziel	1958
book	Cat. Wehvitsch's Afr. Pl.	Hiern	1896
book	For�t du Gabon	Heitz	1943
book	Syst. �d. io	Linn�	1759
book	Ess. for. R�g. mont. Congo orient.	Lebrun	1935
book	Wissensch. Ergebn. Deutsch. Zentral Afr. Exped. 1907-1908	Mildbraed	1914
book	Atlas bois C�te d'Ivoire	Normand	1955
book	Fl. trop. Afr.	Oliver	1868
book	Les drogues simples d'origine v�g�tale	Planch�n et Collin	1896
book	Mati�res premi�res usuelles du r�gne v�g�tal	Perrot	1944
book	Ann. Pharm. franc.	Paris et Moyse-Mignon	1948
book	Fl. Mayombe	Pellegrin	1924
book	Principales cultures du Congo belge, 3e �d.	Van den Abeele et Vandenput	1956
book	Man. ess. for. Congo belge	Vermoesen	1923
book	Pl. utiles Gabon	Walker et Sillans	1961
book	Citrus Industry	Webber et Batchelor	1946
book	Die Pflanzenstoffe	Wehmer	1911
